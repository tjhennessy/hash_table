/**
 * @file primroothash.h
 */

#ifndef PRIMROOTHASH_H
#define PRIMROOTHASH_H

#include <stdint.h>
#include <stdbool.h>

typedef bool prim_root_bool_t;
typedef uint32_t prim_root_num_t;
typedef uint32_t prim_root_hash_t;
typedef uint32_t prim_root_t;
typedef uint32_t prim_root_divisor_t;
typedef uint32_t prim_root_string_sum_t;

/**
 * @brief Takes as an argument a value g, the primitive root, of m
 *        the divisor.  Raises g to the power k, where k is the sum of
 *        the characters in the string val, and returns the remainder
 *        of division with m.
 * 
 *      The neat thing about primitive roots of a number m, is they
 *      are guaranteed to be uniformly distributed between 0 and m - 1.
 *      For cryptographic applications they make great one-way functions
 *      such as the Diffie-Hellman Key Agreement Protocol.
 * 
 *      For non-cryptographic applications they evenly distribute data
 *      across all values 0 to m - 1.  In hashtables this quality is ideal
 *      to avoid clustering of data and to provide a high likelihood that
 *      the actual performance of insert, lookup, and remove will be 
 *      well approximated by the load factor num_elements / size.
 * 
 *      hash value = g^k mod m, where g is primitive root of m and
 *      k is equal to the sum of the characters in the string val.
 * 
 * @see http://mathworld.wolfram.com/PrimitiveRoot.html
 */
prim_root_hash_t prim_root_hash(char *val, prim_root_t g, prim_root_divisor_t m);

/**
 * @brief Determines if p is a prime number.
 * @param p Value to test for primality.
 * @returns true if prime, false otherwise.
 */
prim_root_bool_t prim_root_is_prime(uint32_t p);

/**
 * @brief Determines if p and q are relatively prime.
 * 
 *      Test to see if the greatest common divsior of
 *      integers p and q is equal to 1.  If so, p and q
 *      are relatively prime.
 * 
 * @param p Integer
 * @param q Integer
 * @returns true when p and q are relatively prime, false otherwise
 * @note Assumes as pre-condition arguments are valid.
 */
prim_root_bool_t prim_root_is_rel_prime(uint32_t p, uint32_t q);

/**
 * @brief Computes greatest common divisor of p and q.
 * @param p Integer
 * @param q Integer
 * @returns prim_root_num_t 
 */
prim_root_num_t prim_root_gcd(uint32_t p, uint32_t q);

// TODO: test if g is primitive root of m
// TODO: generate smallest primitive root of m

/**
 * @brief Sum each char in string.
 * 
 *      Go character-by-character in string summing together
 *      the integer values.
 * @param string Character array.
 * @returns Positive sum of characters on success.
 * @note Assumes as pre-condition all arguments are passed correctly.
 */
prim_root_string_sum_t orim_root_sum_chars(char *string);
#endif /* PRIMROOTHASH_H */