/**
 * @file elfhash.h
 */

#ifndef ELFHASH_H
#define ELFHASH_H

#include <stdint.h>

/**
 * @brief A hashing function used in UNIX object files.
 * 
 * @param val String being hashed.
 * @return uint32_t hash of val.
 * 
 * @see https://en.wikipedia.org/wiki/PJW_hash_function
 * 
 * @note The value returned from this function is in no
 *       way ready to be used as an index into a hash table.
 *       Caller must take care to ensure they map the returned
 *       hash value into their range.
 */
uint32_t elfhash(char *val);

#endif /* ELFHASH_H */