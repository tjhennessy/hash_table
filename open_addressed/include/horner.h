/**
 * @file horner.h
 */

#ifndef HORNER_H
#define HORNER_H

#include <stdint.h>

#define COEFFICIENT 117

/**
 * @brief Horner's method improves efficiency of string
 *        hashing methods by eliminating multiplications
 *        within the inner loop of hashing functions.
 * @param val String to be hashed.
 * @param m Prime number representing size of hash table.
 * @return uint32_t hash value
 * @see https://www.cs.princeton.edu/courses/archive/spr03/cs226/lectures/hashing.4up.pdf
 * @see https://www.cs.utexas.edu/~mitra/csSpring2017/cs313/lectures/hash.html
 */
uint32_t horner(char *val, int m);

#endif /* HORNER_H */