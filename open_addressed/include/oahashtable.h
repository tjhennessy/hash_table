/**
 * @file oahashtable.h
 * 
 * NOTE: Any integer return type is represented by oahtbl_result_t
 * type which returns more specific return values.  If a function
 * returns OAHTBL_SUCCESS the interpretation is depedent upon
 * the context of the function.
 * 
 * TODO: Discuss selection of size limit for
 * hash table, the hash functions involved in double
 * hashing, and the reasoning for double hashing.
 */
#ifndef OAHASHTABLE_H
#define OAHASHTABLE_H

#include <stdio.h>

/* ------- OAHTBL_RESULT_T Values ------- */
#define OAHTBL_TBL_FULL        -4
#define OAHTBL_MEM_ERR         -3
#define OAHTBL_NOT_FOUND       -2
#define OAHTBL_FAILURE         -1
#define OAHTBL_SUCCESS          0
#define OAHTBL_ALREADY_IN_TABLE 1

/* -------------- Macros ---------------------- */
#define oahtbl_size(ht) ((ht)->size)
#define oahtbl_element_is_unoccupied(ht, pos) (NULL == ((ht)->table[(pos)]))

/* ----------- Library Types ----------------- */
typedef int oahtbl_result_t;
typedef int oahtbl_dblhash_t;

/* ---------- Function Pointers --------------- */
typedef int  (*hash1_f)(const void *key);
typedef int  (*hash2_f)(const void *key);
typedef int  (*equals_f)(const void *key1, const void *key2);
typedef void (*destroy_f)(void *data);

/* ------------ Structs --------------- */
typedef struct oahashtbl_
{
    int positions;     // total number of spots
    int size;          // current table size, size <= positions
    void *vacated;     // Used when element removed
    void **table;      // actual hash table
    hash1_f h1;        // User must provide two distinct hash functions
    hash2_f h2;        //   for h1 and h2
    equals_f equals;   // User must provide this compare function
    destroy_f destroy; // Only function not required, can be NULL
} oahashtable;

/***********************************************************************
 *                                                                     *
 * WARNING: No parameters are checked, validated.  All parameters are  *
 *          assumed to be passed correctly.                            *
 *                                                                     *
 ***********************************************************************/

/**
 * @brief Function takes a variable for hashtable, allocates memory for
 *        the hashtable, then initializes all fields.
 * @param ht Pointer to oahashtable.
 * @param pos Number of elements in hashtable.
 * @param h1 User defined hash function.
 * @param h2 User defined hash function.
 * @param equals User defined function for comparing two key values.
 * @param destroy User defined destroy function.
 * @returns 0 on success, OAHASHTABLE_MEM_ERR on failure.
 * @see oahashtable.h for definitions.
 * @note The reason for two hash functions is to provide double
 *       hashing.
 * @note User must provide all user defined functions except destroy,
 *       which can be passed NULL.  When destroy is passed as NULL,
 *       the assumption is the elements within the table are will not
 *       have memory freed or there is no dynamically allocated memory
 *       associated with the elements.
 * @note ht structure is zeroed out in order to prevent use after free.
 */
oahtbl_result_t oahtbl_init(oahashtable *ht, 
                int pos,
                hash1_f h1, 
                hash2_f h2, 
                equals_f equals,
                destroy_f destroy);

/**
 * @brief Destroys hashtable by freeing hashtable resources.
 * 
 *      If user provided destroy function to init, then for
 *      each element the user defined destroy function is called
 *      freeing memory and resources associated with each element.
 *      Whether the user provided a destroy function for each element
 *      or not, the memory allocated for the hashtable itself is
 *      freed.
 * 
 * @param ht Pointer to open addressed hash table.
 * @returns Nothing
 */
void oahtbl_destroy(oahashtable *ht);

/**
 * @brief Used to insert a new element into open addressed hash table.
 * 
 *      Attempts to take data and insert it into open addressed hash table
 *      using double hashing strategy.  In order for data to be inserted 
 *      into hash table the following three things must be true:
 *      1- Hash table is not full; positions are available for new element
 *      2- Data key value cannot exist in table already; no duplicates
 *      3- Uniform hashing assumption must hold; double hashing must allow
 *         for every position to be probed.
 * 
 *      If the above invariants hold, an element will be inserted.
 * 
 * @param ht Pointer to a hash table.
 * @param data Constant pointer to data.
 * @returns OAHTBL_TBL_FULL if size limit reached, OAHASHTBL_ALREADY_IN_TABLE
 *          if duplicate, OAHTBL_SUCCESS if inserted, or OAHTBL_FAILURE 
 *          if after all probing attempts element could not be inserted (likely
 *          an issue with the hashing strategy).
 * @note User must manage memory for parameter data.
 * @note Assumes probing takes no more than 1 / (1 - load factor) probes
 */
oahtbl_result_t oahtbl_insert(oahashtable *ht, const void *data);

// TODO: Add comment after implementing
int oahtbl_remove(oahashtable *ht, void **data);

/**
 * @brief Determines if data is in hashtable.
 * 
 *      Uses double hashing to identify location in hashtable of 
 *      argument data.  The position associated with the hash value 
 *      is broken into three cases: 
 *          1- occupied and keys are equal (match), 
 *          2- occupied but keys do not match (collision),
 *          3- unoccupied (add to table).
 * 
 *      When case 1 is encountered, OAHTBL_SUCCESS is returned
 *      functionally and the in/out param data points to data in
 *      hashtable.  
 * 
 *      When case 2 is encountered, an iterative search (linear probes) 
 *      for the position where the element could be has been performed.
 *      Any possibility of the element with equivalent data
 *      being in the hashtable is ruled out.  OAHTBL_NOT_FOUND is
 *      returned.
 * 
 *      When case 3 is encountered, the double hash value returned
 *      points to a position in the hashtable where no element exists
 *      (NULL) and that indicates the element with data has not been
 *      added to the hashtable or has been removed from it.
 * 
 * @param ht A constant pointer to open addressed hashtable.
 * @param data Generic data structure, user provides definition.
 * @returns OAHASHTBL_SUCCESS if found, OAHTBL_NOT_FOUND otherwise
 * @note OAHASHTBL_NOT_FOUND returned in two cases: (1) position data hashes
 *       to in hashtable is empty, or (2) all possible positions data 
 *       hashes to are occupied by other elements which do not match data.
 */
oahtbl_result_t oahtbl_lookup(const oahashtable *ht, void **data);
#endif /* OAHASHTABLE_H */