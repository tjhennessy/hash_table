/**
 * @file elfhash.c
 */

#include <stdio.h>
#include <elfhash.h>

uint32_t elfhash(char *val)
{
    uint32_t h = 0;
    uint32_t high;
    while (*val != '\0')
    {
        h = (h << 4) + *val;
        high = h & 0xF0000000;
        if (high != 0)
        {
            h ^= (high >> 24);
        }
        h &= ~high;
        val++;
    }
    return h;
}
