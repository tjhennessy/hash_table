/**
 * @file oahashtable.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <oahashtable.h>

static char vacated;

static oahtbl_dblhash_t oahtbl_compute_dbl_hash(oahashtable *ht, void *data, int i)
{
    return (ht->h1(data) + (i * ht->h2(data))) % ht->positions;
}
 
oahtbl_result_t oahtbl_init(oahashtable *ht,
                int pos,
                hash1_f h1, 
                hash2_f h2, 
                equals_f equals,
                destroy_f destroy)
{
    ht->table = (void **) malloc(pos * sizeof(void *));
    if (NULL == ht->table)
    {
        return -1;
    }

    ht->vacated = &vacated;
    ht->h1 = h1;
    ht->h2 = h2;
    ht->equals = equals;
    ht->destroy = destroy;
    ht->size = 0;
    ht->positions = pos;

    for (int i = 0; i < ht->positions; i++)
    {
        ht->table[i] = NULL;
    }

    return 0;
}

void oahtbl_destroy(oahashtable *ht)
{
    bool user_has_provided_destroy_function;
    bool current_position_occupied;
    bool is_not_vacated_position;

    user_has_provided_destroy_function = (ht->destroy != NULL);
    if (user_has_provided_destroy_function)
    {
        for (int i = 0; i < ht->positions; i++)
        {
            current_position_occupied = (ht->table[i] != NULL);
            is_not_vacated_position = (ht->table[i] != ht->vacated);
            if (current_position_occupied && is_not_vacated_position)
            {
                ht->destroy(ht->table[i]);
            }
        }
    }
    free(ht->table);
    memset(ht, 0, sizeof(oahashtable));
}

oahtbl_result_t oahtbl_insert(oahashtable *ht, const void *data)
{
    void *tmp_data;
    int pos;
    bool current_position_free;
    bool is_vacated_position;
    
    /* Ensure space exists for inserting */
    if (ht->size == ht->positions)
    {
        return OAHTBL_TBL_FULL;
    }

    /* Ensure duplicates are not added */
    tmp_data = (void *) data;
    if (oahtbl_lookup(ht, &tmp_data) == OAHTBL_SUCCESS)
    {
        return OAHTBL_ALREADY_IN_TABLE;
    }

    /* Probe table for open position */
    for (int i = 0; i < ht->positions; i++)
    {
        pos = oahtbl_compute_dbl_hash(ht, data, i);
        current_position_free = (ht->table[pos] == NULL);
        is_vacated_position = (ht->table[pos] == ht->vacated);
        if (current_position_free || is_vacated_position)
        {
            ht->table[pos] = (void *) data;
            ht->size++;
            return OAHTBL_SUCCESS;
        }
    }

    /* If here, good chance hashing strategy sucks */
    return OAHTBL_FAILURE;
}

// TODO: Implement after insert above
int oahtbl_remove(oahashtable *ht, void **data);

oahtbl_result_t oahtbl_lookup(const oahashtable *ht, void **data)
{
    int i;
    int pos;

    for (int i = 0; i < ht->positions; i++)
    {
        pos = oahtbl_compute_dbl_hash(ht, *data, i);
        if (oahtbl_element_is_unoccupied(ht, pos))
        {   /* Element has not been added or was removed */
            return OAHTBL_NOT_FOUND;
        }
        else if (ht->equals(ht->table[pos], *data))
        {   /* Key's match, return data ASW key in hashtable */
            *data = ht->table[pos];
            return OAHTBL_SUCCESS;
        }
    }

    /* Element not found anywhere in hashtable */
    return OAHTBL_NOT_FOUND;
}
