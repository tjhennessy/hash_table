/**
 * @file horner.c
 */
#include <stdio.h>
#include <horner.h>

uint32_t horner(char *val, int m)
{
    uint32_t h = 0;
    while (*val != '\0')
    {
        h = (COEFFICIENT * h + *val) % m;
        val++;
    }
    return h;
}
